# No 1
https://gitlab.com/frinaldisyauqi/gesture-recognition

Mentranslasikan gerakan gesture tangan yang berasal dari webcam realtime menjadi sebuah inputan pada komputer pada aplikasi yang saya buat kita mentranslasikan gesture tangan menjadi pergerakan dan aksi yang dilakukan mouse/cursor. Menggunakan mediapipe sebagai hand recognition yang membaca pergerakan tangan lalu merancang gesture bagaimana saja yang dapat di translasikan menjadi aksi cursor/mouse.

# No 2
```mermaid
    graph TD
        A(Webcam Feeds) --> B
        B(Gesture Recognition) --> C
        C(Cursor Action & Movement)
```

# No 3
### Gesture Recognition CLI app
<br/>
![demo-image](demo/demo.gif)

# No 4
Link YouTube : https://youtu.be/oXJQNqpVHkw

# No 5

Gesture recognition menggunakan MediaPipe dengan real-time processing menggunakan webcam adalah metode yang sangat efektif untuk mengenali dan memahami gerakan tubuh manusia secara alami dan responsif. Dengan menggunakan MediaPipe sebagai kerangka kerja yang powerful untuk pengolahan video dan visi komputer, serta menghubungkannya dengan webcam sebagai sumber input video, kita dapat dengan mudah mendeteksi gerakan dalam setiap frame video secara berurutan. MediaPipe menyediakan komponen-komponen seperti deteksi pose tubuh atau pengenalan tangan yang dapat digunakan untuk melakukan tugas-tugas tersebut. Kemampuan real-time processing memungkinkan aplikasi kita memberikan respons instan terhadap gerakan yang terdeteksi, seperti menampilkan pesan atau perintah pada layar, mengendalikan objek virtual, atau tindakan lain sesuai dengan kebutuhan aplikasi. Dengan MediaPipe, kita dapat menciptakan antarmuka interaktif yang intuitif dan aplikasi-aplikasi yang kaya akan fitur dalam berbagai bidang seperti game, kontrol gestur, antarmuka pengguna, dan pengenalan bahasa isyarat. Dalam perkembangannya, MediaPipe terus berkembang dan memungkinkan pengembangan sistem yang lebih canggih dan kompleks di masa depan.

# No 6
https://gitlab.com/frinaldisyauqi/gesture-recognition

Mentranslasikan gerakan gesture tangan yang berasal dari webcam realtime menjadi sebuah inputan pada komputer pada aplikasi yang saya buat kita mentranslasikan gesture tangan menjadi pergerakan dan aksi yang dilakukan mouse/cursor. Menggunakan mediapipe sebagai hand recognition yang membaca pergerakan tangan lalu merancang gesture bagaimana saja yang dapat di translasikan menjadi aksi cursor/mouse.


# No 7
### Gesture Recognition CLI app
<br/>
![demo-image](demo/demo.gif)

# No 8
Link YouTube : https://youtu.be/oXJQNqpVHkw

# No 9
Gesture recognition menggunakan MediaPipe dengan real-time processing menggunakan webcam adalah metode yang sangat efektif untuk mengenali dan memahami gerakan tubuh manusia secara alami dan responsif. Dengan menggunakan MediaPipe sebagai kerangka kerja yang powerful untuk pengolahan video dan visi komputer, serta menghubungkannya dengan webcam sebagai sumber input video, kita dapat dengan mudah mendeteksi gerakan dalam setiap frame video secara berurutan. MediaPipe menyediakan komponen-komponen seperti deteksi pose tubuh atau pengenalan tangan yang dapat digunakan untuk melakukan tugas-tugas tersebut. Kemampuan real-time processing memungkinkan aplikasi kita memberikan respons instan terhadap gerakan yang terdeteksi, seperti menampilkan pesan atau perintah pada layar, mengendalikan objek virtual, atau tindakan lain sesuai dengan kebutuhan aplikasi. Dengan MediaPipe, kita dapat menciptakan antarmuka interaktif yang intuitif dan aplikasi-aplikasi yang kaya akan fitur dalam berbagai bidang seperti game, kontrol gestur, antarmuka pengguna, dan pengenalan bahasa isyarat. Dalam perkembangannya, MediaPipe terus berkembang dan memungkinkan pengembangan sistem yang lebih canggih dan kompleks di masa depan.


# No 10
Paper : 
https://docs.google.com/document/d/1bMPb9LcjnTHIu_FSrXmiwKTQgWK8B-VQzxImbt2izVw/edit?usp=sharing